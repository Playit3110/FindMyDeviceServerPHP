class APP {
	constructor() {
		(async function() {
			let version = document.getElementById("version");
			version.innerText = await FMD.version();
		})();

		MAP = new MAP(document.getElementById("map"));

		Gallery = new Gallery(document.getElementById("gallery"));

		let delem = document.getElementById("date");
		this.drange = new DRange(delem, {
			info: true
		});
		this.drange.updateInfo(
			function(left) {
				left = Math.floor(left / 60) * 60;
				return new Date(left * 1000).toLocaleString();
			},
			function(right) {
				right = Math.ceil(right / 60 + 1) * 60;
				return new Date(right * 1000).toLocaleString();
			}
		);

		let that = this;
		delem.onload =
		delem.onchange = function(e) {
			if(e.drange) {
				let {max, min, left, right} = e.drange;

				if(left < min + 10) {
					that.drange.updateConf({
						min: Math.floor(min - 60 * 60)
					});
				}
				MAP.filter({
					min: new Date(left * 1000),
					max: new Date(right * 1000),
				});
				MAP.zoom();
			}
		}
	}

	async locate(e) {
		if(e) e.target.disabled = true;

		let limit = {};
		limit.max = Math.floor(Date.now() / 1000) * 1000;
		limit.min = limit.max - 24 * 60 * 60 * 1000;
		await MAP.filter(limit);
		this.drange.updateConf({
			min: Math.floor(limit.min / 1000),
			max: Math.floor(limit.max / 1000),
			left: "min",
			right: "max"
		});
		document.getElementsByTagName("nav")[0].classList.remove("hidden");

		LOCS.checkNewLocations();

		if(e) e.target.disabled = false;
	}

	sendCommand(cmd) {
		FMD.sendCommand(cmd);
	}
	takePicture(side) {
		Gallery.takePicture(side);
	}
	showPicture(side) {
		Gallery.showPicture(side);
	}

	updateDisplayRange({min, max}) {
		MAP.filter({min, max});
	}
}
