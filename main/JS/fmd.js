const FMD = new (class {
	#id;
	#pw = {
		plain: "",
		hash: "",
		salt: ""
	};
	#key = null;

	async version() {
		return await request("/version", null, "GET").then(res => res.text());
	}

	async _locate(data, showErrors = true) {
		let token = await this.requestAccess();
		if(token == false) {
			// alert("No Access allowed");
			return false;
		}

		let locs = await request("/location", {
			IDT: token,
			Data: data
		}).then(res => res.json());
		if(locs.state == "error") {
			if(showErrors) alert("Error while getting locations");
			return false;
		}
		if(locs.state !== "error" && locs.location.length < 1) {
			if(showErrors) alert("No locations found");
			return false;
		}

		locs = await this.parseLocations(locs.location);

		return locs;
	}

	async locateLast(amount = 3, date = Date.now(), showErrors = true) {
		return await this._locate({
			amount: parseInt(amount),
			max: new Date(date).toISOString()
		}, showErrors);
	}

	async locateDate({min = Date.now() - 60 * 60 * 1000, max = Date.now()}, showErrors = true) {
		return await this._locate({
			min: new Date(min).toISOString(),
			max: new Date(max).toISOString()
		}, showErrors);
	}

	async parseLocations(rawLocs) {
		if(await this.getKey() == false) {
			alert("No key found");
			return false;
		}

		let crypt = new JSEncrypt();
		crypt.setPrivateKey(this.#key);
		for(let [i, loc] of Object.entries(rawLocs)) {
			for(let [attr, prop] of Object.entries(loc)) {
				if(attr == "date") {
					rawLocs[i][attr] = new Date(prop);
					continue;
				}
				rawLocs[i][attr] = crypt.decrypt(prop);
			}
		}

		return rawLocs;
	}

	getID(id = "did") {
		let did = document.getElementById(id).value;
		if(did !== "") {
			this.#id = did;
			return true;
		}
		return false;
	}

	getPW(id = "passwd", returnPW = false) {
		let pw = document.getElementById(id).value;
		if(pw !== "" && this.#pw.salt !== "") {
			if(returnPW) return pw;
			this.#pw.plain = pw;
			this.#pw.hash = CryptoJS.PBKDF2(
				this.#pw.plain,
				CryptoJS.enc.Hex.parse(this.#pw.salt), {
				keySize: 8,
				iterations: 1867 * 2
			}).toString().toUpperCase();
			return true;
		}
		return false;
	}

	async getSalt() {
		if(this.#pw.salt !== "") return true;

		let res = await request("/salt", {
			IDT: this.#id
		}).then(res => res.json());
		if(res.state == "error") {
			alert("Error while getting salt");
			return false;
		}
		if(res.state !== "error" && !res.Data) {
			if(res.message) alert(res.message);
			return false;
		}

		if(res.Data == "cafe") {
			alert("Important: Please update your Password in the APP.");
		}
		this.#pw.salt = res.Data;
		return true;
	}

	async requestAccess() {
		if(
			this.getID() == false ||
			await this.getSalt() == false ||
			this.getPW() == false
		) {
			alert("You need to enter a device-ID and a password");
			return false;
		}

		let res = await request("/requestAccess", {
			IDT: this.#id,
			Data: this.#pw.hash
		}).then(res => res.json());
		if(res.state == "error") {
			alert("Error while getting token");
			return false;
		}
		if(res.state !== "error" && !res.Data) {
			if(res.message) alert(res.message);
			return false;
		}

		return res.Data;
	}

	async getKey() {
		if(this.#key) return true;

		let token = await this.requestAccess();
		if(token == false) return false;

		let enckey = await request("/key", {
			IDT: token,
			Data: ""
		}).then(res => res.text());
		// if(enckey.state == "error") {
		// 	alert("Error while getting token");
		// 	return false;
		// }
		// if(key.state !== "error" && !key.Data) {
		// 	if(key.message) alert(key.message);
		// 	return false;
		// }
		if(enckey !== "") {
			this.#key = this.decryptAES(enckey);
			return true;
		}

		return false;
	}

	decryptAES(c, pass = this.#pw.plain) {
		let keySize = 256;
		let ivSize = 128;
		let iterationCount = 1867;

		let ivLength = ivSize / 4;
		let saltLength = keySize / 4;

		let iv = c.substr(saltLength, ivLength);
		let enc = c.substring(ivLength + saltLength);

		let salt = c.substr(0, saltLength);
		let passkey = CryptoJS.PBKDF2(
			pass,
			CryptoJS.enc.Hex.parse(salt), {
			keySize: keySize / 32,
			iterations: iterationCount
		});

		let cP = CryptoJS.lib.CipherParams.create({
			ciphertext: CryptoJS.enc.Base64.parse(enc)
		});
		let dec = CryptoJS.AES.decrypt(cP, passkey, {
			iv: CryptoJS.enc.Hex.parse(iv)
		});
		try {
			return dec.toString(CryptoJS.enc.Utf8);
		} catch(error) {
			return null;
		}
	}

	async sendCommand(cmd) {
		let token = await this.requestAccess();
		if(token == false) {
			// alert("No Access allowed");
			return false;
		}

		if([
			"locate",
			"ring",
			"lock",
			"delete",
			"camera front",
			"camera back"
		].indexOf(cmd) < 0) return;

		await request("/command", {
			IDT: token,
			Data: cmd
		}, "POST");
		alert("Command send");
	}

	async getPicture(index = -1) {
		let token = await this.requestAccess();
		if(token == false) {
			// alert("No Access allowed");
			return false;
		}

		let res = await request("/picture", {
			IDT: token,
			Data: index
		}).then(res => res.json());
		let pics = [];
		for(let pic of res.pictures) {
			pic = pic.split("___PICTURE-DATA___");

			let crypt = new JSEncrypt();
			crypt.setPrivateKey(this.#key);
			pic[0] = crypt.decrypt(pic[0]);
			pic = this.decryptAES(pic[1], pic[0]);

			pics.push("data:image/jpeg;base64," + pic);
		}

		return pics;
	}

	async delete() {
		let token = await this.requestAccess();
		if(token == false) {
			// alert("No Access allowed");
			return false;
		}

		await request("/device", {
			IDT: token,
			Data: ""
		}, "POST");

		this.#id = undefined;
		this.#pw = {
			plain: "",
			hash: "",
			salt: ""
		};
		this.#key = undefined;
	}
})();
