class DRange {
	constructor(elem, conf = {min: 0, max: 100, step: 1, left: 0, right: 100, info: false}) {
		if(typeof elem == "string")
			elem = document.querySelector(elem);
		if(elem == null || elem == undefined || !(elem instanceof HTMLElement)) {
			console.error("DRange: No valid element or query");
			return;
		}
		this.elem = elem;
		this.elem.innerHTML = "";
		this.elem.classList.add("drange");

		this.initDoubleRange();

		this.conf = {
			min: 0,
			max: 100,
			step: 1,
			left: 0,
			right: 100,
			info: false
		};
		this.updateConf(conf, true);
		this._onHandle({
			type: "load"
		});
	}

	initDoubleRange() {
		function createInput(that) {
			let range = document.createElement("input");
			range.type = "range";
			range.oninput =
			range.onchange = function(e) {
				// let conf = e.target == that.lrange ? {
				// 	left: Math.min(that.lrange.value, that.rrange.value - 1)
				// } : {
				// 	right: Math.max(that.rrange.value, that.lrange.value - (-1))
				// };
				if(e.target == that.lrange) {
					that.conf.left = parseFloat(that.lrange.value);
					that._updateLeft(
						Math.min(that.lrange.value, that.rrange.value - 1)
					);
				} else {
					that.conf.right = parseFloat(that.rrange.value);
					that._updateRight(
						Math.max(that.rrange.value, that.lrange.value - (-1))
					);
				}

				let index = (100 - parseInt(that.range.style.right) - parseInt(that.range.style.left)) < 5 && parseInt(that.range.style.right) < 5;
				that.lrange.style.zIndex = 3 + index;

				that._onHandle(e);
			}
			return range;
		}


		this.slider = document.createElement("div");
		this.slider.className = "slider";
		this.elem.append(this.slider);

		this.lrange = createInput(this);
		this.lrange.className = "lrange";
		this.slider.append(this.lrange);

		this.lthumb = document.createElement("div");
		this.lthumb.className = "lthumb";
		this.slider.append(this.lthumb);

		this.range = document.createElement("div");
		this.range.className = "range";
		this.slider.append(this.range);

		this.rrange = createInput(this);
		this.rrange.className = "rrange";
		this.slider.append(this.rrange);

		this.rthumb = document.createElement("div");
		this.rthumb.className = "rthumb";
		this.slider.append(this.rthumb);


		this.info = document.createElement("div");
		this.info.className = "info";
		this.info.cb = {
			left: v => v,
			right: v => v
		};
		this.elem.append(this.info);

		this.left = document.createElement("span");
		this.left.className = "left";
		this.info.append(this.left);

		this.right = document.createElement("span");
		this.right.className = "right";
		this.info.append(this.right);

		// this.updateConf();
	}

	_onHandle(e) {
		let cb = this.elem["on" + e.type];
		if(typeof cb == "function") {
			let that = this;
			e.drange = {
				get min() {
					return that.conf.min;
				},
				get max() {
					return that.conf.max;
				},
				get step() {
					return that.conf.step;
				},
				get left() {
					return parseInt(that.lrange.value)
				},
				get right() {
					return parseInt(that.rrange.value)
				},
				updateConf: that.updateConf
			};
			cb(e);
			if(e.preventDefault) e.preventDefault();
		}
	}

	updateConf(conf = {min: 0, max: 100, step: 1, left: 0, right: 100, info: false}, init = false) {
		if(["min", "max"].indexOf(conf.right) > -1) {
			conf.right = conf[conf.right];
		}
		if(["min", "max"].indexOf(conf.left) > -1) {
			conf.left = conf[conf.left];
		}

		Object.entries(conf).map(function([k, v]) {
			if(k == "info") return;
			conf[k] = parseFloat(v);
		});

		for(let attr of ["min", "max", "step", "left", "right", "info"]) {
			if(conf.hasOwnProperty(attr))
				this.conf[attr] = conf[attr];

			switch(attr) {
				case "min":
				case "max":
				case "step":
					this.lrange[attr] =
					this.rrange[attr] =
					this.conf[attr];

					this._updateLeft();
					this._updateRight();
					break;
				case "left":
					this._updateLeft();
					break;
				case "right":
					this._updateRight();
					break;
				case "info":
					if(this.conf.info) {
						if(this.info.hasAttribute("style"))
							this.info.removeAttribute("style");
						break;
					}
					this.info.style.display = "none";
					break;
			}
		}
	}

	updateInfo(leftcb, rightcb = null) {
		this.info.cb.left = leftcb;
		this.info.cb.right = rightcb || leftcb;
	}

	_updateRight(right = this.conf.right) {
		this.rrange.value = right;
		let pright = this._toPercent(right);
		this.rthumb.style.left = pright + "%";

		this.range.style.right = (100 - pright) + "%";
		this.right.innerText = this.info.cb.right(right);
	}

	_updateLeft(left = this.conf.left) {
		this.lrange.value = left;
		let pleft = this._toPercent(left);
		this.lthumb.style.left = pleft + "%";

		this.range.style.left = pleft + "%";
		this.left.innerText = this.info.cb.left(left);
	}

	_toPercent(value) {
		if(typeof value !== "number") value = parseFloat(value);
		value = Math.round(value / this.conf.step) * this.conf.step;
		value = (value - this.conf.min) * 100;
		value /= this.conf.max - this.conf.min;
		value = Math.max(0, value);
		value = Math.min(value, 100);
		return value;
	}
}
