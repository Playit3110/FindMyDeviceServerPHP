class Gallery {
	constructor(elem = document.body) {
		if(typeof elem == "string")
			elem = document.querySelector(elem);
		if(elem == null || elem == undefined) {
			console.error("DRange: No valid element or query");
			return;
		}

		/*
		div#gallery.hidden
			button.onclick="...">{close}
			div.title>{Pictures}
			div.imgs
		*/

		this.elem = elem;

		let button = document.createElement("button");
		button.setAttribute(
			"onclick",
			"this.parentElement.classList.add('hidden')"
		);
		button.innerText = "close";
		this.elem.append(button);

		let div = document.createElement("div");
		div.className = "title";
		div.innerText = "Pictures";
		this.elem.append(div);

		this.imgs = document.createElement("div");
		this.imgs.innerHTML = "";
		this.imgs.classList.add("imgs");

		this.initGallery();
	}

	async initGallery(pictures = []) {
		for(let pic of pictures) {
			this.addPicture(pic);
		}
	}

	addPicture(picture) {
		let index = this.imgs.children.length;

		let label = document.createElement("label");
		label.id = "img" + index;
		label.className = "img";
		this.imgs.append(label);

		let input = document.createElement("input");
		input.type = "checkbox";
		input.onclick = function() {
			let cL = document.body.classList;
			if(this.checked) {
				cL.add("noscroll");
				return;
			}
			cL.remove("noscroll");
		}
		input.hidden = true;
		label.append(input);

		let img = document.createElement("img");
		img.src = picture;
		img.alt = "img" + index;
		label.append(img);
	}

	async makePicture(side = "front") {
		await FMD.sendCommand("camera " + side);
		setTimeout(async function() {
			let pic = await FMD.getPicture()[0];
			if(pic) this.addPicture(pic);
		}, 1000);
	}

	async showPictures(e) {
		e.target.disabled = true;
		let pics = await FMD.getPicture("all");
		if(pics)
			gallery.initGallery(pics);

		e.target.disabled = false;
		this.overlay.classList.remove("hidden");
	}
}