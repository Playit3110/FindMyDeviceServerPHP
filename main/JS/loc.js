class LOC {
	constructor(loc) {
		this.loc = loc;
		this.marker = "";
	}

	get date() {
		return new Date(this.loc.date || Date.now());
	}
	get type() {
		return this.loc.provider || "";
	}
	get lon() {
		return this.loc.lon || 0;
	}
	get lat() {
		return this.loc.lat || 0;
	}

	set marker(marker) {
		if(marker?.type !== "marker")
			marker = MAP.makeMarker(this.loc);
		this._marker = marker;
	}
	get marker() {
		return this._marker;
	}
}


let LOCS = new (class {
	constructor() {
		this.locs = [];
	}

	add(nlocs) {
		for(let nloc of nlocs) {
			nloc = new LOC(nloc);
			this.locs.push(nloc);
		}
		this.locs = this.locs.filter((v1, v2) => v1.date !== v2.date || (v1.lat !== v2.lat && v1.lon !== v2.lon));
	}

	getAll() {
		let locs = this.locs;
		locs = locs.sort(function(last, next) {
			return last.date < next.date;
		});

		return locs;
	}

	getByDate({
		locs = this.locs,
		date = new Date()
	}) {
		return locs.filter(loc => loc.date == date);
	}

	async filterByDate({
		locs = this.getAll(),
		min = new Date(Date.now() - 60 * 60 * 1000),
		max = new Date()
	}) {
		if(!(min instanceof Date))
			min = new Date(min);
		if(!(max instanceof Date))
			max = new Date(max);

		//check if enougth data is requested
		if(locs[locs.length - 1].date > min) {
			let nmin = new Date(min);
			let nlocs = false;
			let attemps = 1;
			do {
				nmin.setHours(nmin.getHours() - attemps);
				nlocs = await FMD.locateDate({
					min: nmin,
					max: locs[locs.length - 1].date
				}, false);
			} while(!nlocs && attemps++ < 3);
			if(nlocs) {
				this.add(nlocs);
				locs = this.getAll();
			}
		}

		locs = locs.filter(function(loc) {
			return loc.date > min && loc.date < max;
		});
		return locs;
	}

	filterByType({
		locs = this.getAll(),
		type = []
	}) {
		if(typeof type !== "object") type = [type];
		if(type.length < 1) return locs;

		// type = <gps|OpenCellId-Standort|network|fused>
		let regex = new RegExp("("+type.join("|")+")");
		locs = locs.filter(function(loc) {
			return loc.type.match(regex);
		});
		return locs;
	}

	async filter({
		locs = this.getAll(),
		min = new Date(),
		max = new Date(),
		type = false
	}, reverse = false) {
		if(!(min instanceof Date))
			min = new Date(min);
		if(!(max instanceof Date))
			max = new Date(max);

		if(locs.length < 1) {
			this.add(
				await FMD.locateDate({
					min: min,
					max: max
				})
			);
			locs = this.getAll();
		}

		locs = await this.filterByDate({
			locs: locs,
			min: min,
			max: max
		});

		locs = this.filterByType({
			locs: locs,
			type: type
		});

		if(reverse)
			locs = locs.reverse();

		return locs;
	}


	checkNewLocations() {
		if(this.loop) {
			return;
			// clearInterval(this.loop);
		}

		let lloc = this.getAll()[0];
		this.loop = setInterval(async function() {
			let nloc = await FMD.locateDate({
				min: lloc.date
			}, false);

			if(nloc.length > 0) {
				this.add(nloc);
				lloc = this.getAll()[0];

				APP.drange.updateConf({
					max: Math.floor(lloc.date / 1000)
				});
			}
		}.bind(this), 5 * 60 * 1000);
	}
});
