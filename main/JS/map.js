class MAP {
	constructor(map, conf = {
		center: [0, 0],
		zoom: 1.5
	}) {
		this.map = L.map(map, Object.assign({
			center: [0, 0],
			zoom: 1.5
		}, conf));
		this.map.zoomControl.setPosition("bottomleft");

		L.tileLayer("https://{s}.tile.osm.org/{z}/{x}/{y}.png", {
			attribution: "&copy; <a href=\"https://osm.org/copyright\">OpenStreetMap</a> contributors"
		}).addTo(this.map);

		// MAP.setMaxZoom(20);
		// ^ not usable

		this.locs =
		this.lines = [];

		this.date = {
			min: new Date(),
			max: new Date()
		};

		this.types = {
			gps: false,
			gms: false,
			net: false
		};
	}

	makeMarker(loc) {
		let div = document.createElement("div");
		let date = document.createElement("div");
		date.innerText = "Date: "+loc.date.toLocaleString()
		div.append(date);

		let prov = document.createElement("div");
		prov.innerText = "Provider: "+loc.provider;
		div.append(prov);

		let bat = document.createElement("div");
		bat.innerText = "Battery: "+loc.bat;
		div.append(bat);

		let marker = L.marker([loc.lat, loc.lon]);
		marker.type = "marker";
		marker.bindPopup(div.outerHTML);

		marker.addTo(this.map);
		return marker;
	}

	makeLine({last, next}) {
		let line = L.polyline([
			[last.lat, last.lon],
			[next.lat, next.lon]
		]);
		line.type = "line";

		line.addTo(this.map);
		return line;
	}


	enableGPS(state = false) {
		this.types.gps = state;
		(async function() {
			await this.updateMap();
		}).call(this);
	}

	enableGMS(state = false) {
		this.types.gms = state;
		(async function() {
			await this.updateMap();
		}).call(this);
	}

	enableNetwork(state = false) {
		this.types.net = state;
		(async function() {
			await this.updateMap();
		}).call(this);
	}

	async updateMap() {
		let types = [];
		for(let [type, state] of Object.entries(this.types)) {
			if(state) types.push({
				gps: "gps",
				gms: "OpenCellId-Standort",
				net: "network"
			}[type]);
		}
		if(Object.values(this.types).every(st => st == true)) {
			types = ["fused"];
		}

		this.locs = await LOCS.filter({
			...this.date,
			type: types
		}) || [];

		for(let line of this.lines) {
			line.remove();
		}
		this.lines = [];

		let last;
		let alocs = LOCS.getAll();
		for(let loc of alocs) {
			let melem = loc.marker.getElement();
			if(this.locs.includes(loc)) {
				loc.marker.setOpacity(1);
				melem.style.display = "initial";

				if(last) {
					this.lines.push(
						this.makeLine({
							last: last,
							next: loc
						})
					);
				}

				last = loc;
				continue;
			}

			loc.marker.setOpacity(0);
			melem.style.display = "none";
		}

		this.zoom();
	}

	async filter({min = new Date(), max = new Date()}) {
		if(!(min instanceof Date))
			min = new Date(min);
		if(!(max instanceof Date))
			max = new Date(max);

		this.date = {
			min: min,
			max: max
		};

		await this.updateMap();
	}

	zoom(locs = this.locs) {
		if(locs.length < 1) return;
		locs = locs.map(function(loc) {
			return [loc.lat, loc.lon];
		});

		this.map.fitBounds(
			new L.LatLngBounds(locs)
		);
	}
}
