window.onload = async function() {
	APP = new APP();
}

function request(url, body = "", method = "PUT") {
	return new Promise(function(resolve, reject) {
		if(typeof body == "object") {
			body = JSON.stringify(body);
		}

		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if(this.status == 200 && this.readyState == 4) {
				let that = this;
				resolve({
					text: function() {
						return that.responseText;
					},
					json: function() {
						try {
							return JSON.parse(that.responseText);
						} catch(error) {
							return this.text();
						}
					}
				});
			}
		}
		xhr.open(method, "./" + url);
		if(method !== "GET") {
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(body);
		} else {
			xhr.send();
		}
	});
}



function downloadLocs(locs = map.locations, fancy = false) {
	let keys = Object.keys(locs[0]);
	keys = keys.filter(v => ["marker", "line"].indexOf(v) < 0);
	locs = JSON.stringify(
		locs,
		keys,
		fancy ? "\t" : null
	);

	let f = new File(
		[locs],
		"locs.json"
	);

	let a = document.createElement("a");
	a.download = f.name;
	a.href = URL.createObjectURL(f);
	a.click();
	URL.revokeObjectURL(a.href);
}
