<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["provider", "date", "bat", "lon", "lat", "IDT"])) {
	$username = checkToken($_POST["IDT"]);

	$date = new DateTime();
	$date->createFromFormat("U.v", strval($_POST["date"] / 1000));
	$date = $date->format("Y-m-d H:i:s.v");

	sqlquery("INSERT INTO location(username, provider, lon, lat, bat, date) VALUES (:user, :prov, :lon, :lat, :bat, :date);", [
		":user" => $username,
		":prov" => $_POST["provider"],
		":lon" => $_POST["lon"],
		":lat" => $_POST["lat"],
		":bat" => $_POST["bat"],
		":date" => $date
	]);

	exit;
	response([
		"state" => "ok"
	]);
}

if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	if(is_numeric($_PUT["Data"])) {
		//To Nulide API
		$sql = sqlquery(implode(" ", [
			"SELECT provider, lon, lat, bat, DATE_FORMAT(date, \"%Y-%m-%dT%H:%i:%s\") AS date FROM location",
			"WHERE username = :user",
			"ORDER BY date DESC;"
		]), [
			":user" => $username
		]);
		$sql = $sql[$_PUT["Data"]];

		response([
			"state" => "ok",
			"location" => $sql
		]);
	}

	$DATA = $_PUT["Data"];
	if(
		array_key_exists("amount", $DATA) &&
		is_numeric($DATA["amount"]) &&
		array_key_exists("max", $DATA)
	) {
		$amount = intval($DATA["amount"]);
		if(!is_int($amount) || $amount < 1) response([
			"state" => "error",
			"message" => "Amount is not a positiv integer"
		]);

		$max = new DateTime($DATA["max"]);

		$sql = sqlquery(implode(" ", [
			"SELECT provider, lon, lat, bat, DATE_FORMAT(date, \"%Y-%m-%dT%H:%i:%sZ\") AS date FROM location",
			"WHERE username = :user",
			"AND date < :max",
			"ORDER BY date DESC",
			"LIMIT :amount;"
		]), [
			":user" => $username,
			":max" => $max->format("Y-m-d H:i:s"),
			":amount" => $amount
		]);

		response([
			"state" => "ok",
			"location" => $sql
		]);
	}

	if(
		array_key_exists("min", $DATA) &&
		array_key_exists("max", $DATA)
	) {
		$date = $_PUT["Data"];
		if(!exists($date, ["min", "max"], true)) response([
			"state" => "error",
			"message" => "min or max date are missing"
		]);

		$date["min"] = new DateTime($date["min"]);
		$date["max"] = new DateTime($date["max"]);

		$sql = sqlquery(implode(" ", [
			"SELECT provider, lon, lat, bat, DATE_FORMAT(date, \"%Y-%m-%dT%H:%i:%sZ\") AS date FROM location",
			"WHERE username = :user AND date > :min AND date < :max",
			"ORDER BY date DESC;"
		]), [
			":user" => $username,
			":min" => $date["min"]->format("Y-m-d H:i:s"),
			":max" => $date["max"]->format("Y-m-d H:i:s")
		]);

		response([
			"state" => "ok",
			"location" => $sql
		]);
	}

	response([
		"state" => "error",
		"message" => "Data is missing: amount and min or max, min and max"
	]);
}
?>