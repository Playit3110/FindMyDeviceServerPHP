<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "hashedPassword", "salt", "privkey"])) {
	$username = checkToken($_PUT["IDT"]);

	sqlquery("UPDATE user SET password = :pw, salt = :salt, pri = :pri WHERE username = :user;", [
		":user" => $username,
		":pw" => $_PUT["hashedPassword"],
		":salt" => $_PUT["salt"],
		":pri" => $_PUT["privkey"]
	]);

	response([
		"state" => "ok",
		"IDT" => $_PUT["IDT"],
		"Data" => true
	]);
}
?>
