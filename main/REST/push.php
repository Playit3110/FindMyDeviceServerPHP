<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["IDT", "Data"])) {
	$username = checkToken($_POST["IDT"]);

	sqlquery("UPDATE user SET cmdLink = :link WHERE username = :user;", [
		":link" => $_POST["Data"],
		":user" => $username
	]);

	response([
		"state" => "ok"
	]);
}
?>