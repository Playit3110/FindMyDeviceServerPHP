<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, "IDT")) {
	checkValidID($_PUT["IDT"]);
	$username = $_PUT["IDT"];

	$salt = "cafe";
	$sql = sqlquery("SELECT salt FROM user WHERE username = :user;", [
		":user" => $username
	])[0];
	if(exists($sql))
		$salt = $sql["salt"];

	response([
		"state" => "ok",
		"IDT" => $_PUT["IDT"],
		"Data" => $salt
	]);
}
?>