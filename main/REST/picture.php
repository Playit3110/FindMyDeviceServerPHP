<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["IDT", "Data"])) {
	$username = checkToken($_POST["IDT"]);

	sqlquery("INSERT INTO picture(username, pic) VALUES (:user, :pic);", [
		":user" => $username,
		":pic" => $_POST["Data"]
	]);

	exit;
	response([
		"state" => "ok"
	]);
}

if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	$sql = sqlquery("SELECT pic FROM picture WHERE username = :user;", [
		":user" => $username
	]);

	$pics = array_map(function($e) {
		return $e["pic"];
	}, $sql);
	if(is_numeric($_PUT["Data"])) {
		$index = $_PUT["Data"];
		while($index < 0) $index += count($pics);
		$index %= count($pics);

		$pics = [$pics[$index]];
	}

	response([
		"state" => "ok",
		"pictures" => $pics
	]);
	// ^TODO: find array keys and values
}
?>