<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	$sql = sqlquery("SELECT pub FROM user WHERE username = :user;", [
		":user" => $username
	])[0];
	$key = $sql["pub"];
	// $key = base64_encode($key);

	response([
		"state" => "ok",
		"IDT" => $_PUT["IDT"],
		"Data" => $key
	]);
}
?>