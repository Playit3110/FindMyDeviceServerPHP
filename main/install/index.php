<?php
error_reporting(E_ALL);
ini_set("display_errors", "1");

include_once(dirname(__DIR__)."/conf.php");
include_once(dirname(__DIR__)."/main.php");

function displayLog($data) {
	echo "$data\n";
}

function showTable($table, $columns) {
	echo "CREATE TABLE $table (\n\t".implode(
		",\n\t",
		$columns
	)."\n);\n";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Install</title>
</head>
<body>
	<h1>Install</h1>
	<pre><?php
		$tables = file_get_contents(__DIR__."/tables.json");
		$tables = json_decode($tables, true);
		foreach($tables as $table) {
			$keys = [];
			$columns = [];
			foreach($table["structure"] as $key => $column) {
				$keys[] = $key;
				$columns[$key] = "$key $column";
			}
			$extra = $table["extra"];
			$table = $table["name"];

			try {
				displayLog("creating $table");
				showTable($table, $columns);

				sqlquery("CREATE TABLE $table (".implode(
					", ",
					array_merge($columns, $extra)
				).") DEFAULT CHARSET = utf8mb4;");
			} catch(\Throwable $th) {
				// table exists already - update table
				displayLog("updating $table");

				$ccolumns = sqlquery("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :table;", [
					":table" => $table
				]);
				$ccolumns = array_map(function($v) {
					return $v["COLUMN_NAME"];
				}, $ccolumns);

				$dkeys = array_merge(
					array_diff(array_keys($columns), $ccolumns),
					array_diff($ccolumns, array_keys($columns))
				);
				foreach($dkeys as $key) {
					if(in_array($key, $ccolumns)) {
						displayLog("removing column: $key");
						sqlquery("ALTER TABLE $table DROP COLUMN $key;");
						continue;
					}
					displayLog("adding column: $key");
					$prevkey = array_search($key, $keys);
					$prevkey = $prevkey ? " AFTER ".$keys[$prevkey - 1] : "";
					sqlquery("ALTER TABLE $table ADD ".$columns[$key]."$prevkey;");
				}
			}
			displayLog(str_repeat("-", 80));
		}

		$it = new RecursiveDirectoryIterator(
			__DIR__,
			RecursiveDirectoryIterator::SKIP_DOTS
		);
		$fofs = new RecursiveIteratorIterator(
			$it,
			RecursiveIteratorIterator::CHILD_FIRST
		);
		foreach($fofs as $fof) {
			$path = $fof->getRealPath();
			displayLog("removing install: ".str_replace($_SERVER["DOCUMENT_ROOT"], "", $path)); 
			if($fof->isDir()){
				rmdir($path);
			} else {
				unlink($path);
			}
		}
		rmdir(__DIR__);
		?>
	</pre>
</body>
</html>