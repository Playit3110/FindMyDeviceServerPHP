<?php
$ROOT = $_SERVER["DOCUMENT_ROOT"];
$VERSION = "0.3.5";
$TLENGTH = 20;
$ULENGTH = 10;

try {
	$PDO = new PDO(
		"mysql:dbname=DBNAME;host=127.0.0.1;charset=utf8",
		"DBUSERNAME",
		"DBPASSWORD"
	);

	$PDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(\Throwable $th) {
	die("ERROR: No SQL connection. Configure your SQL connection in conf.php.");
}
?>
